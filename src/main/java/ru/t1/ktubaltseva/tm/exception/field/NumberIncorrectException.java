package ru.t1.ktubaltseva.tm.exception.field;

import org.jetbrains.annotations.NotNull;

public class NumberIncorrectException extends AbstractFieldException {

    public NumberIncorrectException() {
        super("Error! Number is incorrect...");
    }

    public NumberIncorrectException(@NotNull final String value) {
        super("Error! This number value \"" + value + "\" is incorrect...");
    }

}