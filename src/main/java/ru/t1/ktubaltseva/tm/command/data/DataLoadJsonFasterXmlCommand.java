package ru.t1.ktubaltseva.tm.command.data;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.ktubaltseva.tm.dto.Domain;
import ru.t1.ktubaltseva.tm.exception.data.LoadDataException;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

public class DataLoadJsonFasterXmlCommand extends AbstractDataCommand {

    @NotNull
    private final String NAME = "data-load-json-faster-xml";

    @NotNull
    private final String DESC = "Load data from json file.";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESC;
    }

    @Override
    public void execute() throws LoadDataException {
        System.out.println("[LOAD JSON DATA]");
        try {
            @NotNull final byte[] bytes = Files.readAllBytes(Paths.get(FILE_JSON));
            @Nullable final String json = new String(bytes);
            @NotNull final ObjectMapper objectMapper = new ObjectMapper();
            @NotNull final Domain domain = objectMapper.readValue(json, Domain.class);
            setDomain(domain);
        } catch (IOException e) {
            throw new LoadDataException();
        }
    }

}
