package ru.t1.ktubaltseva.tm.component;

import org.jetbrains.annotations.NotNull;
import ru.t1.ktubaltseva.tm.command.data.AbstractDataCommand;
import ru.t1.ktubaltseva.tm.exception.AbstractException;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.security.NoSuchAlgorithmException;

public class Backup extends Thread {

    @NotNull
    private final Bootstrap bootstrap;

    public Backup(@NotNull final Bootstrap bootstrap) {
        this.bootstrap = bootstrap;
        this.setDaemon(true);
    }

    public void init() throws AbstractException, NoSuchAlgorithmException {
        load();
        start();
    }

    public void save() throws AbstractException, NoSuchAlgorithmException {
        bootstrap.processCommand(AbstractDataCommand.COMMAND_BACKUP_SAVE, false);
    }

    public void load() throws AbstractException, NoSuchAlgorithmException {
        if (!Files.exists(Paths.get(AbstractDataCommand.FILE_BACKUP))) return;
        bootstrap.processCommand(AbstractDataCommand.COMMAND_BACKUP_LOAD, false);
    }

    @Override
    public void run() {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                Thread.sleep(3000);
                save();
            } catch (AbstractException | NoSuchAlgorithmException | InterruptedException e) {
                bootstrap.getLoggerService().error(e);
            }
        }
    }


}
